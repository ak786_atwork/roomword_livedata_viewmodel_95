package com.example.anil.roomword_livedata_viewmodel_95;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "word_table")
public class Word {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "word")
    private String mWord;           //@PrimaryKey(autoGenerate = true)

    public Word(String word) {this.mWord = word;}

    //Every field that's stored in the database needs to be either public or have a "getter" method. This sample provides a getWord() method.
    public String getWord(){return this.mWord;}
}